export default [
	{
    path: '/',
    component: () => import('@/pages/index'),
	},
	{
    path: '/about',
    component: () => import('@/pages/about'),
	},
	{
    path: '/contacts',
    component: () => import('@/pages/contacts'),
	},

	

  { // 404
    path: '*',
    component: () => import('@/pages/404')
  }
]